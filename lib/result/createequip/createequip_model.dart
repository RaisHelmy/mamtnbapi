import '/backend/api_requests/api_calls.dart';
import '/components/textfield_widget.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import '/flutter_flow/upload_data.dart';
import '/custom_code/widgets/index.dart' as custom_widgets;
import '/flutter_flow/custom_functions.dart' as functions;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CreateequipModel extends FlutterFlowModel {
  ///  Local state fields for this page.

  bool? addclassheader = false;

  List<dynamic> classinfo = [];
  void addToClassinfo(dynamic item) => classinfo.add(item);
  void removeFromClassinfo(dynamic item) => classinfo.remove(item);
  void removeAtIndexFromClassinfo(int index) => classinfo.removeAt(index);
  void updateClassinfoAtIndex(int index, Function(dynamic) updateFn) =>
      classinfo[index] = updateFn(classinfo[index]);

  List<FFUploadedFile> uploadedfile = [];
  void addToUploadedfile(FFUploadedFile item) => uploadedfile.add(item);
  void removeFromUploadedfile(FFUploadedFile item) => uploadedfile.remove(item);
  void removeAtIndexFromUploadedfile(int index) => uploadedfile.removeAt(index);
  void updateUploadedfileAtIndex(
          int index, Function(FFUploadedFile) updateFn) =>
      uploadedfile[index] = updateFn(uploadedfile[index]);

  bool? addmainwork = false;

  List<String> charcodes = [];
  void addToCharcodes(String item) => charcodes.add(item);
  void removeFromCharcodes(String item) => charcodes.remove(item);
  void removeAtIndexFromCharcodes(int index) => charcodes.removeAt(index);
  void updateCharcodesAtIndex(int index, Function(String) updateFn) =>
      charcodes[index] = updateFn(charcodes[index]);

  List<String> charvalues = [];
  void addToCharvalues(String item) => charvalues.add(item);
  void removeFromCharvalues(String item) => charvalues.remove(item);
  void removeAtIndexFromCharvalues(int index) => charvalues.removeAt(index);
  void updateCharvaluesAtIndex(int index, Function(String) updateFn) =>
      charvalues[index] = updateFn(charvalues[index]);

  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();
  // Stores action output result for [Backend Call - API (CreateEquipment)] action in Button widget.
  ApiCallResponse? createdequipment;
  // State field(s) for eqktuorequipmentdescription widget.
  TextEditingController? eqktuorequipmentdescriptionController;
  String? Function(BuildContext, String?)?
      eqktuorequipmentdescriptionControllerValidator;
  // State field(s) for object widget.
  String? objectValue;
  FormFieldController<String>? objectValueController;
  // State field(s) for inbdtyear widget.
  String? inbdtyearValue;
  FormFieldController<String>? inbdtyearValueController;
  // State field(s) for inbdtmonth widget.
  String? inbdtmonthValue;
  FormFieldController<String>? inbdtmonthValueController;
  // State field(s) for inbdtday widget.
  String? inbdtdayValue;
  FormFieldController<String>? inbdtdayValueController;
  // State field(s) for herstormanufacturer widget.
  TextEditingController? herstormanufacturerController;
  String? Function(BuildContext, String?)?
      herstormanufacturerControllerValidator;
  // State field(s) for typbzormodelnumber widget.
  TextEditingController? typbzormodelnumberController;
  String? Function(BuildContext, String?)?
      typbzormodelnumberControllerValidator;
  // State field(s) for sergeormanufacturerserialnumber widget.
  TextEditingController? sergeormanufacturerserialnumberController;
  String? Function(BuildContext, String?)?
      sergeormanufacturerserialnumberControllerValidator;
  // State field(s) for dropdowncountrycodeofmanufacturer widget.
  String? dropdowncountrycodeofmanufacturerValue;
  FormFieldController<String>? dropdowncountrycodeofmanufacturerValueController;
  // State field(s) for baujj widget.
  String? baujjValue;
  FormFieldController<String>? baujjValueController;
  // State field(s) for baumm widget.
  String? baummValue;
  FormFieldController<String>? baummValueController;
  // State field(s) for anlnrorassetno widget.
  TextEditingController? anlnrorassetnoController1;
  String? Function(BuildContext, String?)? anlnrorassetnoController1Validator;
  // State field(s) for anlnrorassetno widget.
  TextEditingController? anlnrorassetnoController2;
  String? Function(BuildContext, String?)? anlnrorassetnoController2Validator;
  // State field(s) for plannergroup widget.
  String? plannergroupValue;
  FormFieldController<String>? plannergroupValueController;
  // State field(s) for mainworkcenter widget.
  String? mainworkcenterValue;
  FormFieldController<String>? mainworkcenterValueController;
  // State field(s) for DropDown widget.
  String? dropDownValue1;
  FormFieldController<String>? dropDownValueController1;
  // State field(s) for DropDown widget.
  String? dropDownValue2;
  FormFieldController<String>? dropDownValueController2;
  // State field(s) for DropDown widget.
  String? dropDownValue3;
  FormFieldController<String>? dropDownValueController3;
  // Stores action output result for [Backend Call - API (GetBay)] action in DropDown widget.
  ApiCallResponse? apiResultvfb;
  // State field(s) for DropDown widget.
  String? dropDownValue4;
  FormFieldController<String>? dropDownValueController4;
  // State field(s) for DropDown widget.
  String? dropDownValue5;
  FormFieldController<String>? dropDownValueController5;
  // State field(s) for DropDown widget.
  String? dropDownValue6;
  FormFieldController<String>? dropDownValueController6;
  // State field(s) for functionallocationdescription widget.
  TextEditingController? functionallocationdescriptionController;
  String? Function(BuildContext, String?)?
      functionallocationdescriptionControllerValidator;
  // State field(s) for classheader widget.
  String? classheaderValue;
  FormFieldController<String>? classheaderValueController;
  // Stores action output result for [Backend Call - API (GetClassInfo)] action in classheader widget.
  ApiCallResponse? apiResult28e;
  // Models for textfield dynamic component.
  late FlutterFlowDynamicModels<TextfieldModel> textfieldModels;
  bool isDataUploading1 = false;
  FFUploadedFile uploadedLocalFile1 =
      FFUploadedFile(bytes: Uint8List.fromList([]));

  bool isDataUploading2 = false;
  FFUploadedFile uploadedLocalFile2 =
      FFUploadedFile(bytes: Uint8List.fromList([]));

  /// Initialization and disposal methods.

  void initState(BuildContext context) {
    textfieldModels = FlutterFlowDynamicModels(() => TextfieldModel());
  }

  void dispose() {
    unfocusNode.dispose();
    eqktuorequipmentdescriptionController?.dispose();
    herstormanufacturerController?.dispose();
    typbzormodelnumberController?.dispose();
    sergeormanufacturerserialnumberController?.dispose();
    anlnrorassetnoController1?.dispose();
    anlnrorassetnoController2?.dispose();
    functionallocationdescriptionController?.dispose();
    textfieldModels.dispose();
  }

  /// Action blocks are added here.

  /// Additional helper methods are added here.
}
