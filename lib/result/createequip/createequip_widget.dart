import '/backend/api_requests/api_calls.dart';
import '/components/textfield_widget.dart';
import '/flutter_flow/flutter_flow_drop_down.dart';
import '/flutter_flow/flutter_flow_icon_button.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import '/flutter_flow/flutter_flow_widgets.dart';
import '/flutter_flow/form_field_controller.dart';
import '/flutter_flow/upload_data.dart';
import '/custom_code/widgets/index.dart' as custom_widgets;
import '/flutter_flow/custom_functions.dart' as functions;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'createequip_model.dart';
export 'createequip_model.dart';

class CreateequipWidget extends StatefulWidget {
  const CreateequipWidget({Key? key}) : super(key: key);

  @override
  _CreateequipWidgetState createState() => _CreateequipWidgetState();
}

class _CreateequipWidgetState extends State<CreateequipWidget> {
  late CreateequipModel _model;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _model = createModel(context, () => CreateequipModel());

    _model.eqktuorequipmentdescriptionController ??= TextEditingController();
    _model.herstormanufacturerController ??= TextEditingController();
    _model.typbzormodelnumberController ??= TextEditingController();
    _model.sergeormanufacturerserialnumberController ??=
        TextEditingController();
    _model.anlnrorassetnoController1 ??= TextEditingController();
    _model.anlnrorassetnoController2 ??= TextEditingController();
    _model.functionallocationdescriptionController ??= TextEditingController();
  }

  @override
  void dispose() {
    _model.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<FFAppState>();

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(_model.unfocusNode),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
        appBar: AppBar(
          backgroundColor: FlutterFlowTheme.of(context).primary,
          automaticallyImplyLeading: false,
          leading: FlutterFlowIconButton(
            borderColor: Colors.transparent,
            borderRadius: 30.0,
            borderWidth: 1.0,
            buttonSize: 60.0,
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
              size: 30.0,
            ),
            onPressed: () async {
              context.pop();
            },
          ),
          title: Text(
            'Create Equipment',
            style: FlutterFlowTheme.of(context).headlineMedium.override(
                  fontFamily: 'Outfit',
                  color: Colors.white,
                  fontSize: 22.0,
                ),
          ),
          actions: [],
          centerTitle: true,
          elevation: 2.0,
        ),
        body: SafeArea(
          top: true,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Align(
                  alignment: AlignmentDirectional(1.0, 0.0),
                  child: Padding(
                    padding:
                        EdgeInsetsDirectional.fromSTEB(20.0, 20.0, 20.0, 20.0),
                    child: FFButtonWidget(
                      onPressed: () async {
                        _model.createdequipment =
                            await CreateEquipmentCall.call(
                          photosList: _model.uploadedfile,
                          urlendpoint: FFAppState().urlendpoint,
                          queryparam: functions.encodeparameter(
                              getJsonField(
                                FFAppState().alluserinfo,
                                r'''$.EmployeeID''',
                              ).toString(),
                              valueOrDefault<String>(
                                functions.flfromdropdown(
                                    _model.dropDownValue3,
                                    _model.dropDownValue4,
                                    _model.dropDownValue5,
                                    _model.dropDownValue6),
                                'Null',
                              ),
                              _model
                                  .functionallocationdescriptionController.text,
                              _model.eqktuorequipmentdescriptionController.text,
                              _model.herstormanufacturerController.text,
                              _model.typbzormodelnumberController.text,
                              _model.sergeormanufacturerserialnumberController
                                  .text,
                              _model.objectValue,
                              _model.objectValue,
                              '${_model.inbdtyearValue}${_model.inbdtmonthValue}${_model.inbdtdayValue}',
                              _model.baujjValue,
                              _model.baummValue,
                              _model.dropdowncountrycodeofmanufacturerValue,
                              _model.dropdowncountrycodeofmanufacturerValue !=
                                          null &&
                                      _model.dropdowncountrycodeofmanufacturerValue !=
                                          ''
                                  ? getJsonField(
                                      FFAppState()
                                          .country
                                          .where((e) =>
                                              getJsonField(
                                                e,
                                                r'''$.Code''',
                                              ) ==
                                              _model
                                                  .dropdowncountrycodeofmanufacturerValue)
                                          .toList()
                                          .first,
                                      r'''$.Name''',
                                    ).toString()
                                  : null,
                              _model.mainworkcenterValue != null &&
                                      _model.mainworkcenterValue != ''
                                  ? getJsonField(
                                      FFAppState()
                                          .mainworkcenter
                                          .where((e) =>
                                              getJsonField(
                                                e,
                                                r'''$.Code''',
                                              ) ==
                                              _model.mainworkcenterValue)
                                          .toList()
                                          .first,
                                      r'''$.BusinessArea''',
                                    ).toString()
                                  : null,
                              _model.anlnrorassetnoController1.text,
                              _model.anlnrorassetnoController2.text,
                              _model.mainworkcenterValue != null &&
                                      _model.mainworkcenterValue != ''
                                  ? getJsonField(
                                      FFAppState()
                                          .mainworkcenter
                                          .where((e) =>
                                              getJsonField(
                                                e,
                                                r'''$.Code''',
                                              ) ==
                                              _model.mainworkcenterValue)
                                          .toList()
                                          .first,
                                      r'''$.CostCenter''',
                                    ).toString()
                                  : null,
                              _model.plannergroupValue,
                              _model.plannergroupValue != null &&
                                      _model.plannergroupValue != ''
                                  ? getJsonField(
                                      FFAppState()
                                          .getplannergroup
                                          .where((e) =>
                                              getJsonField(
                                                e,
                                                r'''$.Code''',
                                              ) ==
                                              _model.plannergroupValue)
                                          .toList()
                                          .first,
                                      r'''$.Name''',
                                    ).toString()
                                  : null,
                              _model.mainworkcenterValue,
                              _model.mainworkcenterValue != null &&
                                      _model.mainworkcenterValue != ''
                                  ? getJsonField(
                                      FFAppState()
                                          .mainworkcenter
                                          .where((e) =>
                                              getJsonField(
                                                e,
                                                r'''$.Code''',
                                              ) ==
                                              _model.mainworkcenterValue)
                                          .toList()
                                          .first,
                                      r'''$.Name''',
                                    ).toString()
                                  : null,
                              null,
                              _model.classheaderValue,
                              _model.classinfo
                                  .map((e) => getJsonField(
                                        e,
                                        r'''$.CharID''',
                                      ))
                                  .toList()
                                  .map((e) => e.toString())
                                  .toList(),
                              functions
                                  .fromlisttolist(
                                      _model.textfieldModels
                                          .getValues(
                                            (m) => m.dropDownValue,
                                          )
                                          .toList(),
                                      _model.textfieldModels
                                          .getValues(
                                            (m) => m.textController.text,
                                          )
                                          .toList())
                                  ?.toList()),
                        );
                        if ((_model.createdequipment?.succeeded ?? true)) {
                          await showDialog(
                            context: context,
                            builder: (alertDialogContext) {
                              return AlertDialog(
                                title: Text('Success'),
                                content: Text('Request has been submitted'),
                                actions: [
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(alertDialogContext),
                                    child: Text('Ok'),
                                  ),
                                ],
                              );
                            },
                          );
                          context.safePop();
                        } else {
                          await showDialog(
                            context: context,
                            builder: (alertDialogContext) {
                              return AlertDialog(
                                title: Text('Error'),
                                content: Text(
                                    'Request has Error.Status Code:${(_model.createdequipment?.statusCode ?? 200).toString()}'),
                                actions: [
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(alertDialogContext),
                                    child: Text('Ok'),
                                  ),
                                ],
                              );
                            },
                          );

                          context.pushNamed('Home');
                        }

                        setState(() {});
                      },
                      text: 'Save',
                      options: FFButtonOptions(
                        height: 40.0,
                        padding: EdgeInsetsDirectional.fromSTEB(
                            24.0, 0.0, 24.0, 0.0),
                        iconPadding:
                            EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 0.0, 0.0),
                        color: FlutterFlowTheme.of(context).primary,
                        textStyle:
                            FlutterFlowTheme.of(context).titleSmall.override(
                                  fontFamily: 'Readex Pro',
                                  color: Colors.white,
                                ),
                        elevation: 3.0,
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Align(
                        alignment: AlignmentDirectional(0.0, -1.0),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Align(
                                alignment: AlignmentDirectional(0.0, -1.0),
                                child: Container(
                                  width: 1080.0,
                                  height: 1900.0,
                                  decoration: BoxDecoration(),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Container(
                                        width: 400.0,
                                        height: 300.0,
                                        decoration: BoxDecoration(
                                          color: FlutterFlowTheme.of(context)
                                              .secondaryBackground,
                                        ),
                                        child: Stack(
                                          children: [
                                            Image.network(
                                              '${FFAppState().urlendpoint}/EquipmentPhoto/testPhoto/energy-electricity_transmission_lines-min.jpg',
                                              width: 500.0,
                                              height: double.infinity,
                                              fit: BoxFit.cover,
                                            ),
                                            Align(
                                              alignment: AlignmentDirectional(
                                                  0.0, 0.0),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.black,
                                                      shape: BoxShape.rectangle,
                                                    ),
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.max,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              EdgeInsetsDirectional
                                                                  .fromSTEB(
                                                                      5.0,
                                                                      5.0,
                                                                      5.0,
                                                                      0.0),
                                                          child: Text(
                                                            'Equipment Detail',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: FlutterFlowTheme
                                                                    .of(context)
                                                                .titleMedium
                                                                .override(
                                                                  fontFamily:
                                                                      'Readex Pro',
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsetsDirectional
                                                                  .fromSTEB(
                                                                      5.0,
                                                                      0.0,
                                                                      5.0,
                                                                      5.0),
                                                          child: Text(
                                                            'New',
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: FlutterFlowTheme
                                                                    .of(context)
                                                                .headlineMedium
                                                                .override(
                                                                  fontFamily:
                                                                      'Outfit',
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize:
                                                                      24.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w900,
                                                                ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Align(
                                          alignment:
                                              AlignmentDirectional(0.0, -1.0),
                                          child: Padding(
                                            padding:
                                                EdgeInsetsDirectional.fromSTEB(
                                                    20.0, 0.0, 20.0, 0.0),
                                            child: DefaultTabController(
                                              length: 4,
                                              initialIndex: 0,
                                              child: Column(
                                                children: [
                                                  Align(
                                                    alignment:
                                                        Alignment(0.0, 0),
                                                    child: TabBar(
                                                      isScrollable: true,
                                                      labelColor:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .primaryText,
                                                      unselectedLabelColor:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .secondaryText,
                                                      labelStyle:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .bodyMedium
                                                              .override(
                                                                fontFamily:
                                                                    'Readex Pro',
                                                                fontSize: 14.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                      indicatorColor:
                                                          FlutterFlowTheme.of(
                                                                  context)
                                                              .primary,
                                                      tabs: [
                                                        Tab(
                                                          text: 'GENERAL',
                                                        ),
                                                        Tab(
                                                          text: 'ORGANIZATION',
                                                        ),
                                                        Tab(
                                                          text: 'CLASS INFO',
                                                        ),
                                                        Tab(
                                                          text: 'PHOTOS',
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: TabBarView(
                                                      physics:
                                                          const NeverScrollableScrollPhysics(),
                                                      children: [
                                                        Align(
                                                          alignment:
                                                              AlignmentDirectional(
                                                                  0.0, -1.0),
                                                          child: Column(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Align(
                                                                alignment:
                                                                    AlignmentDirectional(
                                                                        0.0,
                                                                        -1.0),
                                                                child:
                                                                    SingleChildScrollView(
                                                                  primary:
                                                                      false,
                                                                  child: Column(
                                                                    mainAxisSize:
                                                                        MainAxisSize
                                                                            .max,
                                                                    children: [
                                                                      Align(
                                                                        alignment: AlignmentDirectional(
                                                                            -1.0,
                                                                            -1.0),
                                                                        child:
                                                                            Padding(
                                                                          padding: EdgeInsetsDirectional.fromSTEB(
                                                                              20.0,
                                                                              20.0,
                                                                              20.0,
                                                                              20.0),
                                                                          child:
                                                                              Text(
                                                                            'Details',
                                                                            style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                  fontFamily: 'Readex Pro',
                                                                                  fontSize: 32.0,
                                                                                ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment:
                                                                    AlignmentDirectional(
                                                                        0.0,
                                                                        -1.0),
                                                                child: Column(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .max,
                                                                  children: [
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2557636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Padding(
                                                                        padding: EdgeInsetsDirectional.fromSTEB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0),
                                                                        child:
                                                                            Column(
                                                                          mainAxisSize:
                                                                              MainAxisSize.min,
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          children: [
                                                                            Align(
                                                                              alignment: AlignmentDirectional(-1.0, 0.0),
                                                                              child: Text(
                                                                                'EQUIPMENT',
                                                                                style: FlutterFlowTheme.of(context).bodyMedium,
                                                                              ),
                                                                            ),
                                                                            Align(
                                                                              alignment: AlignmentDirectional(-1.0, 0.0),
                                                                              child: Text(
                                                                                'New',
                                                                                style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                      fontFamily: 'Readex Pro',
                                                                                      fontWeight: FontWeight.w300,
                                                                                    ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Description',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, -1.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                                    child: TextFormField(
                                                                                      controller: _model.eqktuorequipmentdescriptionController,
                                                                                      autofocus: true,
                                                                                      obscureText: false,
                                                                                      decoration: InputDecoration(
                                                                                        labelText: 'Description',
                                                                                        labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        enabledBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: Color(0x2757636C),
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).primary,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        errorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedErrorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                      ),
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      validator: _model.eqktuorequipmentdescriptionControllerValidator.asValidator(context),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Object Type',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: FlutterFlowDropDown<String>(
                                                                                      controller: _model.objectValueController ??= FormFieldController<String>(
                                                                                        _model.objectValue ??= '',
                                                                                      ),
                                                                                      options: FFAppState()
                                                                                          .objecttype
                                                                                          .map((e) => getJsonField(
                                                                                                e,
                                                                                                r'''$.EQART''',
                                                                                              ))
                                                                                          .toList()
                                                                                          .map((e) => e.toString())
                                                                                          .toList(),
                                                                                      optionLabels: FFAppState()
                                                                                          .objecttype
                                                                                          .map((e) => getJsonField(
                                                                                                e,
                                                                                                r'''$.ObjectTypeText''',
                                                                                              ))
                                                                                          .toList()
                                                                                          .map((e) => e.toString())
                                                                                          .toList(),
                                                                                      onChanged: (val) => setState(() => _model.objectValue = val),
                                                                                      height: 30.0,
                                                                                      searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                      textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      hintText: 'Object Type',
                                                                                      searchHintText: 'Search for an item...',
                                                                                      icon: Icon(
                                                                                        Icons.keyboard_arrow_down_rounded,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        size: 24.0,
                                                                                      ),
                                                                                      elevation: 2.0,
                                                                                      borderColor: Color(0x2757636C),
                                                                                      borderWidth: 1.0,
                                                                                      borderRadius: 5.0,
                                                                                      margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                      hidesUnderline: true,
                                                                                      isSearchable: true,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Startup Date',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: FlutterFlowDropDown<String>(
                                                                                      controller: _model.inbdtyearValueController ??= FormFieldController<String>(null),
                                                                                      options: functions.generatefromyear(1950),
                                                                                      onChanged: (val) => setState(() => _model.inbdtyearValue = val),
                                                                                      height: 30.0,
                                                                                      searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                      textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      hintText: 'Year',
                                                                                      searchHintText: 'Search for an item...',
                                                                                      icon: Icon(
                                                                                        Icons.keyboard_arrow_down_rounded,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        size: 24.0,
                                                                                      ),
                                                                                      elevation: 2.0,
                                                                                      borderColor: Color(0x2757636C),
                                                                                      borderWidth: 1.0,
                                                                                      borderRadius: 5.0,
                                                                                      margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                      hidesUnderline: true,
                                                                                      isSearchable: true,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: FlutterFlowDropDown<String>(
                                                                                      controller: _model.inbdtmonthValueController ??= FormFieldController<String>(
                                                                                        _model.inbdtmonthValue ??= '',
                                                                                      ),
                                                                                      options: functions
                                                                                          .generatemonth()
                                                                                          .map((e) => getJsonField(
                                                                                                e,
                                                                                                r'''$.value''',
                                                                                              ))
                                                                                          .toList()
                                                                                          .map((e) => e.toString())
                                                                                          .toList(),
                                                                                      optionLabels: functions
                                                                                          .generatemonth()
                                                                                          .map((e) => getJsonField(
                                                                                                e,
                                                                                                r'''$.name''',
                                                                                              ))
                                                                                          .toList()
                                                                                          .map((e) => e.toString())
                                                                                          .toList(),
                                                                                      onChanged: (val) => setState(() => _model.inbdtmonthValue = val),
                                                                                      height: 30.0,
                                                                                      searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                      textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      hintText: 'Month',
                                                                                      searchHintText: 'Search for an item...',
                                                                                      icon: Icon(
                                                                                        Icons.keyboard_arrow_down_rounded,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        size: 24.0,
                                                                                      ),
                                                                                      elevation: 2.0,
                                                                                      borderColor: Color(0x2757636C),
                                                                                      borderWidth: 1.0,
                                                                                      borderRadius: 5.0,
                                                                                      margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                      hidesUnderline: true,
                                                                                      isSearchable: true,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: FlutterFlowDropDown<String>(
                                                                                      controller: _model.inbdtdayValueController ??= FormFieldController<String>(null),
                                                                                      options: functions.generateday(
                                                                                          valueOrDefault<String>(
                                                                                            _model.baujjValue,
                                                                                            '2023',
                                                                                          ),
                                                                                          valueOrDefault<String>(
                                                                                            _model.inbdtmonthValue,
                                                                                            '12',
                                                                                          )),
                                                                                      onChanged: (val) => setState(() => _model.inbdtdayValue = val),
                                                                                      height: 30.0,
                                                                                      searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                      textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      hintText: 'Day',
                                                                                      searchHintText: 'Search for an item...',
                                                                                      icon: Icon(
                                                                                        Icons.keyboard_arrow_down_rounded,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        size: 24.0,
                                                                                      ),
                                                                                      elevation: 2.0,
                                                                                      borderColor: Color(0x2757636C),
                                                                                      borderWidth: 1.0,
                                                                                      borderRadius: 5.0,
                                                                                      margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                      hidesUnderline: true,
                                                                                      isSearchable: true,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Manufacturer',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, -1.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                                    child: TextFormField(
                                                                                      controller: _model.herstormanufacturerController,
                                                                                      autofocus: true,
                                                                                      obscureText: false,
                                                                                      decoration: InputDecoration(
                                                                                        labelText: 'Manufacturer/ Maker',
                                                                                        labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        enabledBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: Color(0x2757636C),
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).primary,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        errorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedErrorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                      ),
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      validator: _model.herstormanufacturerControllerValidator.asValidator(context),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Model Number',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, -1.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                                    child: TextFormField(
                                                                                      controller: _model.typbzormodelnumberController,
                                                                                      autofocus: true,
                                                                                      obscureText: false,
                                                                                      decoration: InputDecoration(
                                                                                        labelText: 'Model Number',
                                                                                        labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        enabledBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: Color(0x2757636C),
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).primary,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        errorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedErrorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                      ),
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      validator: _model.typbzormodelnumberControllerValidator.asValidator(context),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Manufacturer Serial Number',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, -1.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                                    child: TextFormField(
                                                                                      controller: _model.sergeormanufacturerserialnumberController,
                                                                                      autofocus: true,
                                                                                      obscureText: false,
                                                                                      decoration: InputDecoration(
                                                                                        labelText: 'Description',
                                                                                        labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                        enabledBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: Color(0x2757636C),
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).primary,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        errorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        focusedErrorBorder: UnderlineInputBorder(
                                                                                          borderSide: BorderSide(
                                                                                            color: FlutterFlowTheme.of(context).error,
                                                                                            width: 2.0,
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                                        ),
                                                                                        contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                      ),
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      validator: _model.sergeormanufacturerserialnumberControllerValidator.asValidator(context),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Country of Manufacturer',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: FlutterFlowDropDown<String>(
                                                                                      controller: _model.dropdowncountrycodeofmanufacturerValueController ??= FormFieldController<String>(
                                                                                        _model.dropdowncountrycodeofmanufacturerValue ??= '',
                                                                                      ),
                                                                                      options: FFAppState()
                                                                                          .country
                                                                                          .map((e) => getJsonField(
                                                                                                e,
                                                                                                r'''$.Code''',
                                                                                              ))
                                                                                          .toList()
                                                                                          .map((e) => e.toString())
                                                                                          .toList(),
                                                                                      optionLabels: functions.combinelist(
                                                                                          FFAppState()
                                                                                              .country
                                                                                              .map((e) => getJsonField(
                                                                                                    e,
                                                                                                    r'''$.Code''',
                                                                                                  ))
                                                                                              .toList()
                                                                                              .map((e) => e.toString())
                                                                                              .toList(),
                                                                                          FFAppState()
                                                                                              .country
                                                                                              .map((e) => getJsonField(
                                                                                                    e,
                                                                                                    r'''$.Name''',
                                                                                                  ))
                                                                                              .toList()
                                                                                              .map((e) => e.toString())
                                                                                              .toList(),
                                                                                          '  -  ')!,
                                                                                      onChanged: (val) => setState(() => _model.dropdowncountrycodeofmanufacturerValue = val),
                                                                                      height: 30.0,
                                                                                      searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                      textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                      hintText: 'Country',
                                                                                      searchHintText: 'Search for an item...',
                                                                                      icon: Icon(
                                                                                        Icons.keyboard_arrow_down_rounded,
                                                                                        color: FlutterFlowTheme.of(context).secondaryText,
                                                                                        size: 24.0,
                                                                                      ),
                                                                                      elevation: 2.0,
                                                                                      borderColor: Color(0x2757636C),
                                                                                      borderWidth: 1.0,
                                                                                      borderRadius: 5.0,
                                                                                      margin: EdgeInsetsDirectional.fromSTEB(0.0, 4.0, 16.0, 4.0),
                                                                                      hidesUnderline: true,
                                                                                      isSearchable: true,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(5.0),
                                                                        border:
                                                                            Border.all(
                                                                          color:
                                                                              Color(0x2757636C),
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.max,
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                Column(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Align(
                                                                                  alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                  child: Text(
                                                                                    'Construction Year/Month',
                                                                                    style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                              child: FlutterFlowDropDown<String>(
                                                                                controller: _model.baujjValueController ??= FormFieldController<String>(null),
                                                                                options: functions.generatefromyear(1950),
                                                                                onChanged: (val) => setState(() => _model.baujjValue = val),
                                                                                height: 30.0,
                                                                                searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                hintText: 'Year',
                                                                                searchHintText: 'Search for an item...',
                                                                                icon: Icon(
                                                                                  Icons.keyboard_arrow_down_rounded,
                                                                                  color: FlutterFlowTheme.of(context).secondaryText,
                                                                                  size: 24.0,
                                                                                ),
                                                                                elevation: 2.0,
                                                                                borderColor: Color(0x2757636C),
                                                                                borderWidth: 1.0,
                                                                                borderRadius: 5.0,
                                                                                margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                hidesUnderline: true,
                                                                                isSearchable: true,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                              child: FlutterFlowDropDown<String>(
                                                                                controller: _model.baummValueController ??= FormFieldController<String>(
                                                                                  _model.baummValue ??= '',
                                                                                ),
                                                                                options: functions
                                                                                    .generatemonth()
                                                                                    .map((e) => getJsonField(
                                                                                          e,
                                                                                          r'''$.value''',
                                                                                        ))
                                                                                    .toList()
                                                                                    .map((e) => e.toString())
                                                                                    .toList(),
                                                                                optionLabels: functions
                                                                                    .generatemonth()
                                                                                    .map((e) => getJsonField(
                                                                                          e,
                                                                                          r'''$.name''',
                                                                                        ))
                                                                                    .toList()
                                                                                    .map((e) => e.toString())
                                                                                    .toList(),
                                                                                onChanged: (val) => setState(() => _model.baummValue = val),
                                                                                height: 30.0,
                                                                                searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                hintText: 'Month',
                                                                                searchHintText: 'Search for an item...',
                                                                                icon: Icon(
                                                                                  Icons.keyboard_arrow_down_rounded,
                                                                                  color: FlutterFlowTheme.of(context).secondaryText,
                                                                                  size: 24.0,
                                                                                ),
                                                                                elevation: 2.0,
                                                                                borderColor: Color(0x2757636C),
                                                                                borderWidth: 1.0,
                                                                                borderRadius: 5.0,
                                                                                margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                hidesUnderline: true,
                                                                                isSearchable: true,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Align(
                                                          alignment:
                                                              AlignmentDirectional(
                                                                  0.0, -1.0),
                                                          child: Column(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            children: [
                                                              Align(
                                                                alignment:
                                                                    AlignmentDirectional(
                                                                        -1.0,
                                                                        -1.0),
                                                                child: Padding(
                                                                  padding: EdgeInsetsDirectional
                                                                      .fromSTEB(
                                                                          20.0,
                                                                          20.0,
                                                                          20.0,
                                                                          20.0),
                                                                  child: Text(
                                                                    'Details',
                                                                    style: FlutterFlowTheme.of(
                                                                            context)
                                                                        .bodyMedium
                                                                        .override(
                                                                          fontFamily:
                                                                              'Readex Pro',
                                                                          fontSize:
                                                                              32.0,
                                                                        ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Column(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .max,
                                                                children: [
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2557636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Business Area',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Auto Populated based on Main Work Center',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                    fontFamily: 'Readex Pro',
                                                                                    fontWeight: FontWeight.w300,
                                                                                  ),
                                                                            ),
                                                                          ),
                                                                          if (_model.addmainwork ??
                                                                              true)
                                                                            Align(
                                                                              alignment: AlignmentDirectional(0.0, 0.0),
                                                                              child: Text(
                                                                                valueOrDefault<String>(
                                                                                  getJsonField(
                                                                                    FFAppState()
                                                                                        .mainworkcenter
                                                                                        .where((e) =>
                                                                                            getJsonField(
                                                                                              e,
                                                                                              r'''$.Code''',
                                                                                            ) ==
                                                                                            _model.mainworkcenterValue)
                                                                                        .toList()
                                                                                        .first,
                                                                                    r'''$.BusinessArea''',
                                                                                  ).toString(),
                                                                                  'null',
                                                                                ),
                                                                                style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                      fontFamily: 'Readex Pro',
                                                                                      fontSize: 16.0,
                                                                                    ),
                                                                              ),
                                                                            ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Asset',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, -1.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                              child: TextFormField(
                                                                                controller: _model.anlnrorassetnoController1,
                                                                                autofocus: true,
                                                                                obscureText: false,
                                                                                decoration: InputDecoration(
                                                                                  labelText: 'Asset No',
                                                                                  labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  enabledBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: Color(0x2757636C),
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).primary,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  errorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedErrorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                ),
                                                                                style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                validator: _model.anlnrorassetnoController1Validator.asValidator(context),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Asset Sub No',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, -1.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                              child: TextFormField(
                                                                                controller: _model.anlnrorassetnoController2,
                                                                                autofocus: true,
                                                                                obscureText: false,
                                                                                decoration: InputDecoration(
                                                                                  labelText: 'Asset Sub No',
                                                                                  labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  enabledBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: Color(0x2757636C),
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).primary,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  errorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedErrorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                ),
                                                                                style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                validator: _model.anlnrorassetnoController2Validator.asValidator(context),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Cost Center',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Auto Populated based on Main Work Center',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                    fontFamily: 'Readex Pro',
                                                                                    fontWeight: FontWeight.w300,
                                                                                  ),
                                                                            ),
                                                                          ),
                                                                          if (_model.addmainwork ??
                                                                              true)
                                                                            Align(
                                                                              alignment: AlignmentDirectional(0.0, 0.0),
                                                                              child: Text(
                                                                                valueOrDefault<String>(
                                                                                  getJsonField(
                                                                                    FFAppState()
                                                                                        .mainworkcenter
                                                                                        .where((e) =>
                                                                                            getJsonField(
                                                                                              e,
                                                                                              r'''$.Code''',
                                                                                            ) ==
                                                                                            _model.mainworkcenterValue)
                                                                                        .toList()
                                                                                        .first,
                                                                                    r'''$.CostCenter''',
                                                                                  ).toString(),
                                                                                  'null',
                                                                                ),
                                                                                style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                      fontFamily: 'Readex Pro',
                                                                                      fontSize: 16.0,
                                                                                    ),
                                                                              ),
                                                                            ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Column(
                                                                      mainAxisSize:
                                                                          MainAxisSize
                                                                              .max,
                                                                      children: [
                                                                        Padding(
                                                                          padding: EdgeInsetsDirectional.fromSTEB(
                                                                              10.0,
                                                                              10.0,
                                                                              10.0,
                                                                              10.0),
                                                                          child:
                                                                              Column(
                                                                            mainAxisSize:
                                                                                MainAxisSize.min,
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.center,
                                                                            children: [
                                                                              Align(
                                                                                alignment: AlignmentDirectional(-1.0, 0.0),
                                                                                child: Text(
                                                                                  'Planner Group',
                                                                                  style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        Align(
                                                                          alignment: AlignmentDirectional(
                                                                              -1.0,
                                                                              0.0),
                                                                          child:
                                                                              Padding(
                                                                            padding: EdgeInsetsDirectional.fromSTEB(
                                                                                10.0,
                                                                                10.0,
                                                                                10.0,
                                                                                10.0),
                                                                            child:
                                                                                FlutterFlowDropDown<String>(
                                                                              controller: _model.plannergroupValueController ??= FormFieldController<String>(
                                                                                _model.plannergroupValue ??= '',
                                                                              ),
                                                                              options: FFAppState()
                                                                                  .getplannergroup
                                                                                  .map((e) => getJsonField(
                                                                                        e,
                                                                                        r'''$.Code''',
                                                                                      ))
                                                                                  .toList()
                                                                                  .map((e) => e.toString())
                                                                                  .toList(),
                                                                              optionLabels: functions.combinelist(
                                                                                  FFAppState()
                                                                                      .getplannergroup
                                                                                      .map((e) => getJsonField(
                                                                                            e,
                                                                                            r'''$.Code''',
                                                                                          ))
                                                                                      .toList()
                                                                                      .map((e) => e.toString())
                                                                                      .toList(),
                                                                                  FFAppState()
                                                                                      .getplannergroup
                                                                                      .map((e) => getJsonField(
                                                                                            e,
                                                                                            r'''$.Name''',
                                                                                          ))
                                                                                      .toList()
                                                                                      .map((e) => e.toString())
                                                                                      .toList(),
                                                                                  '  -  ')!,
                                                                              onChanged: (val) => setState(() => _model.plannergroupValue = val),
                                                                              height: 30.0,
                                                                              searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                              textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                              hintText: 'Planner Group',
                                                                              searchHintText: 'Search for an item...',
                                                                              icon: Icon(
                                                                                Icons.keyboard_arrow_down_rounded,
                                                                                color: FlutterFlowTheme.of(context).secondaryText,
                                                                                size: 24.0,
                                                                              ),
                                                                              elevation: 2.0,
                                                                              borderColor: Color(0x2757636C),
                                                                              borderWidth: 1.0,
                                                                              borderRadius: 5.0,
                                                                              margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                              hidesUnderline: true,
                                                                              isSearchable: true,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Main Work Center',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                              child: FlutterFlowDropDown<String>(
                                                                                controller: _model.mainworkcenterValueController ??= FormFieldController<String>(
                                                                                  _model.mainworkcenterValue ??= 'null',
                                                                                ),
                                                                                options: FFAppState()
                                                                                    .mainworkcenter
                                                                                    .map((e) => getJsonField(
                                                                                          e,
                                                                                          r'''$.Code''',
                                                                                        ))
                                                                                    .toList()
                                                                                    .map((e) => e.toString())
                                                                                    .toList(),
                                                                                optionLabels: functions.combinelist(
                                                                                    FFAppState()
                                                                                        .mainworkcenter
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.Code''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    FFAppState()
                                                                                        .mainworkcenter
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.Name''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    '  -  ')!,
                                                                                onChanged: (val) async {
                                                                                  setState(() => _model.mainworkcenterValue = val);
                                                                                  setState(() {
                                                                                    _model.addmainwork = true;
                                                                                  });
                                                                                },
                                                                                height: 30.0,
                                                                                searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                hintText: 'Main Work Center',
                                                                                searchHintText: 'Search for an item...',
                                                                                icon: Icon(
                                                                                  Icons.keyboard_arrow_down_rounded,
                                                                                  color: FlutterFlowTheme.of(context).secondaryText,
                                                                                  size: 24.0,
                                                                                ),
                                                                                elevation: 2.0,
                                                                                borderColor: Color(0x2757636C),
                                                                                borderWidth: 1.0,
                                                                                borderRadius: 5.0,
                                                                                margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                hidesUnderline: true,
                                                                                isSearchable: true,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Functional Location',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Column(
                                                                            mainAxisSize:
                                                                                MainAxisSize.max,
                                                                            children: [
                                                                              Column(
                                                                                mainAxisSize: MainAxisSize.max,
                                                                                children: [
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController1 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue1 ??= '',
                                                                                    ),
                                                                                    options: FFAppState()
                                                                                        .zone
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.ID''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    optionLabels: FFAppState()
                                                                                        .zone
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.Value''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    onChanged: (val) => setState(() => _model.dropDownValue1 = val),
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Zone',
                                                                                    searchHintText: 'Search for Zone...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController2 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue2 ??= '',
                                                                                    ),
                                                                                    options: FFAppState()
                                                                                        .subzone
                                                                                        .where((e) => valueOrDefault<bool>(
                                                                                              valueOrDefault<bool>(
                                                                                                        _model.dropDownValue1 != null && _model.dropDownValue1 != '',
                                                                                                        true,
                                                                                                      ) &&
                                                                                                      (_model.dropDownValue1 != 'GMHQ')
                                                                                                  ? valueOrDefault<bool>(
                                                                                                      getJsonField(
                                                                                                            e,
                                                                                                            r'''$.ZoneCode''',
                                                                                                          ) ==
                                                                                                          _model.dropDownValue1,
                                                                                                      true,
                                                                                                    )
                                                                                                  : true,
                                                                                              true,
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.INGRP''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    optionLabels: functions.combinelist(
                                                                                        FFAppState()
                                                                                            .subzone
                                                                                            .where((e) => valueOrDefault<bool>(
                                                                                                  valueOrDefault<bool>(
                                                                                                            _model.dropDownValue1 != null && _model.dropDownValue1 != '',
                                                                                                            true,
                                                                                                          ) &&
                                                                                                          (_model.dropDownValue1 != 'GMHQ')
                                                                                                      ? valueOrDefault<bool>(
                                                                                                          getJsonField(
                                                                                                                e,
                                                                                                                r'''$.ZoneCode''',
                                                                                                              ) ==
                                                                                                              _model.dropDownValue1,
                                                                                                          true,
                                                                                                        )
                                                                                                      : true,
                                                                                                  true,
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.INGRP''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        FFAppState()
                                                                                            .subzone
                                                                                            .where((e) => valueOrDefault<bool>(
                                                                                                  valueOrDefault<bool>(
                                                                                                            _model.dropDownValue1 != null && _model.dropDownValue1 != '',
                                                                                                            true,
                                                                                                          ) &&
                                                                                                          (_model.dropDownValue1 != 'GMHQ')
                                                                                                      ? valueOrDefault<bool>(
                                                                                                          getJsonField(
                                                                                                                e,
                                                                                                                r'''$.ZoneCode''',
                                                                                                              ) ==
                                                                                                              _model.dropDownValue1,
                                                                                                          true,
                                                                                                        )
                                                                                                      : true,
                                                                                                  true,
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.INNAM''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        '  -  ')!,
                                                                                    onChanged: (val) => setState(() => _model.dropDownValue2 = val),
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Subzone',
                                                                                    searchHintText: 'Search for an item...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController3 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue3 ??= '',
                                                                                    ),
                                                                                    options: FFAppState()
                                                                                        .substation
                                                                                        .where((e) => _model.dropDownValue2 != null && _model.dropDownValue2 != ''
                                                                                            ? (getJsonField(
                                                                                                  e,
                                                                                                  r'''$.INGRP''',
                                                                                                ) ==
                                                                                                _model.dropDownValue2)
                                                                                            : true)
                                                                                        .toList()
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.FL''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    optionLabels: functions.combinelist(
                                                                                        FFAppState()
                                                                                            .substation
                                                                                            .where((e) => _model.dropDownValue2 != null && _model.dropDownValue2 != ''
                                                                                                ? (getJsonField(
                                                                                                      e,
                                                                                                      r'''$.INGRP''',
                                                                                                    ) ==
                                                                                                    _model.dropDownValue2)
                                                                                                : true)
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.FL''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        FFAppState()
                                                                                            .substation
                                                                                            .where((e) => _model.dropDownValue2 != null && _model.dropDownValue2 != ''
                                                                                                ? (getJsonField(
                                                                                                      e,
                                                                                                      r'''$.INGRP''',
                                                                                                    ) ==
                                                                                                    _model.dropDownValue2)
                                                                                                : true)
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.SubstationDesc''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        '  -  ')!,
                                                                                    onChanged: (val) async {
                                                                                      setState(() => _model.dropDownValue3 = val);
                                                                                      _model.apiResultvfb = await GetBayCall.call(
                                                                                        substationFL: _model.dropDownValue3,
                                                                                        urlendpoint: FFAppState().urlendpoint,
                                                                                      );
                                                                                      if ((_model.apiResultvfb?.succeeded ?? true)) {
                                                                                        setState(() {
                                                                                          FFAppState().bay = getJsonField(
                                                                                            (_model.apiResultvfb?.jsonBody ?? ''),
                                                                                            r'''$[:]''',
                                                                                          )!
                                                                                              .toList()
                                                                                              .cast<dynamic>();
                                                                                        });
                                                                                      }

                                                                                      setState(() {});
                                                                                    },
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Substation',
                                                                                    searchHintText: 'Search for an item...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController4 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue4 ??= '',
                                                                                    ),
                                                                                    options: FFAppState().bay.length >= 1
                                                                                        ? FFAppState()
                                                                                            .bay
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.TPLNR''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList()
                                                                                        : [],
                                                                                    optionLabels: FFAppState().bay.length >= 1
                                                                                        ? functions.combinelist(
                                                                                            FFAppState()
                                                                                                .bay
                                                                                                .map((e) => getJsonField(
                                                                                                      e,
                                                                                                      r'''$.TPLNR''',
                                                                                                    ))
                                                                                                .toList()
                                                                                                .map((e) => e.toString())
                                                                                                .toList(),
                                                                                            FFAppState()
                                                                                                .bay
                                                                                                .map((e) => getJsonField(
                                                                                                      e,
                                                                                                      r'''$.BayDesc''',
                                                                                                    ))
                                                                                                .toList()
                                                                                                .map((e) => e.toString())
                                                                                                .toList(),
                                                                                            '  -  ')!
                                                                                        : [],
                                                                                    onChanged: (val) => setState(() => _model.dropDownValue4 = val),
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Bay',
                                                                                    searchHintText: 'Search for an item...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController5 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue5 ??= '',
                                                                                    ),
                                                                                    options: [
                                                                                      'AA',
                                                                                      'AB',
                                                                                      ''
                                                                                    ],
                                                                                    optionLabels: [
                                                                                      'AA  -  Primary',
                                                                                      'AB  -  Secondary',
                                                                                      'ALL'
                                                                                    ],
                                                                                    onChanged: (val) => setState(() => _model.dropDownValue5 = val),
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Primary / Secondary',
                                                                                    searchHintText: 'Search for an item...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  FlutterFlowDropDown<String>(
                                                                                    controller: _model.dropDownValueController6 ??= FormFieldController<String>(
                                                                                      _model.dropDownValue6 ??= '',
                                                                                    ),
                                                                                    options: FFAppState()
                                                                                        .objecttype
                                                                                        .where((e) =>
                                                                                            getJsonField(
                                                                                              e,
                                                                                              r'''$.ObjectTypePrefix''',
                                                                                            ) !=
                                                                                            '')
                                                                                        .toList()
                                                                                        .map((e) => getJsonField(
                                                                                              e,
                                                                                              r'''$.ObjectTypePrefix''',
                                                                                            ))
                                                                                        .toList()
                                                                                        .map((e) => e.toString())
                                                                                        .toList(),
                                                                                    optionLabels: functions.combinelist(
                                                                                        FFAppState()
                                                                                            .objecttype
                                                                                            .where((e) =>
                                                                                                getJsonField(
                                                                                                  e,
                                                                                                  r'''$.ObjectTypePrefix''',
                                                                                                ) !=
                                                                                                '')
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.ObjectTypePrefix''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        FFAppState()
                                                                                            .objecttype
                                                                                            .where((e) =>
                                                                                                getJsonField(
                                                                                                  e,
                                                                                                  r'''$.ObjectTypePrefix''',
                                                                                                ) !=
                                                                                                '')
                                                                                            .toList()
                                                                                            .map((e) => getJsonField(
                                                                                                  e,
                                                                                                  r'''$.ObjectTypeText''',
                                                                                                ))
                                                                                            .toList()
                                                                                            .map((e) => e.toString())
                                                                                            .toList(),
                                                                                        '  -  ')!,
                                                                                    onChanged: (val) => setState(() => _model.dropDownValue6 = val),
                                                                                    width: 300.0,
                                                                                    height: 50.0,
                                                                                    searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                    textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                    hintText: 'Object Type',
                                                                                    searchHintText: 'Search for an item...',
                                                                                    icon: Icon(
                                                                                      Icons.keyboard_arrow_down_rounded,
                                                                                      color: FlutterFlowTheme.of(context).secondaryText,
                                                                                      size: 24.0,
                                                                                    ),
                                                                                    fillColor: FlutterFlowTheme.of(context).primaryBackground,
                                                                                    elevation: 1.0,
                                                                                    borderColor: Color(0xFFC5C5C5),
                                                                                    borderWidth: 1.0,
                                                                                    borderRadius: 5.0,
                                                                                    margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                    hidesUnderline: true,
                                                                                    isSearchable: true,
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(0.0, 10.0, 0.0, 0.0),
                                                                                    child: Text(
                                                                                      valueOrDefault<String>(
                                                                                        functions.flfromdropdown(_model.dropDownValue3, _model.dropDownValue4, _model.dropDownValue5, _model.dropDownValue6),
                                                                                        'Null',
                                                                                      ),
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                            fontFamily: 'Readex Pro',
                                                                                            fontSize: 20.0,
                                                                                          ),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5.0),
                                                                      border:
                                                                          Border
                                                                              .all(
                                                                        color: Color(
                                                                            0x2757636C),
                                                                        width:
                                                                            1.0,
                                                                      ),
                                                                    ),
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsetsDirectional.fromSTEB(
                                                                          10.0,
                                                                          10.0,
                                                                          10.0,
                                                                          10.0),
                                                                      child:
                                                                          Column(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: [
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, 0.0),
                                                                            child:
                                                                                Text(
                                                                              'Functional Location Description',
                                                                              style: FlutterFlowTheme.of(context).bodyMedium,
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment:
                                                                                AlignmentDirectional(-1.0, -1.0),
                                                                            child:
                                                                                Padding(
                                                                              padding: EdgeInsetsDirectional.fromSTEB(0.0, 0.0, 8.0, 0.0),
                                                                              child: TextFormField(
                                                                                controller: _model.functionallocationdescriptionController,
                                                                                autofocus: true,
                                                                                obscureText: false,
                                                                                decoration: InputDecoration(
                                                                                  labelText: 'Functional Location Description',
                                                                                  labelStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  hintStyle: FlutterFlowTheme.of(context).labelMedium,
                                                                                  enabledBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: Color(0x2757636C),
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).primary,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  errorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  focusedErrorBorder: UnderlineInputBorder(
                                                                                    borderSide: BorderSide(
                                                                                      color: FlutterFlowTheme.of(context).error,
                                                                                      width: 2.0,
                                                                                    ),
                                                                                    borderRadius: BorderRadius.circular(8.0),
                                                                                  ),
                                                                                  contentPadding: EdgeInsetsDirectional.fromSTEB(20.0, 0.0, 0.0, 0.0),
                                                                                ),
                                                                                style: FlutterFlowTheme.of(context).bodyMedium,
                                                                                validator: _model.functionallocationdescriptionControllerValidator.asValidator(context),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Column(
                                                          mainAxisSize:
                                                              MainAxisSize.max,
                                                          children: [
                                                            Align(
                                                              alignment:
                                                                  AlignmentDirectional(
                                                                      0.0,
                                                                      -1.0),
                                                              child:
                                                                  SingleChildScrollView(
                                                                child: Column(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .min,
                                                                  children: [
                                                                    Align(
                                                                      alignment: AlignmentDirectional(
                                                                          -1.0,
                                                                          -1.0),
                                                                      child:
                                                                          Padding(
                                                                        padding: EdgeInsetsDirectional.fromSTEB(
                                                                            20.0,
                                                                            20.0,
                                                                            20.0,
                                                                            20.0),
                                                                        child:
                                                                            Text(
                                                                          'Details',
                                                                          style: FlutterFlowTheme.of(context)
                                                                              .bodyMedium
                                                                              .override(
                                                                                fontFamily: 'Readex Pro',
                                                                                fontSize: 32.0,
                                                                              ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Align(
                                                                      alignment:
                                                                          AlignmentDirectional(
                                                                              1.0,
                                                                              -1.0),
                                                                      child:
                                                                          FFButtonWidget(
                                                                        onPressed:
                                                                            () async {
                                                                          setState(
                                                                              () {
                                                                            _model.addclassheader =
                                                                                true;
                                                                          });
                                                                        },
                                                                        text:
                                                                            'Add Class Header',
                                                                        options:
                                                                            FFButtonOptions(
                                                                          height:
                                                                              40.0,
                                                                          padding: EdgeInsetsDirectional.fromSTEB(
                                                                              24.0,
                                                                              0.0,
                                                                              24.0,
                                                                              0.0),
                                                                          iconPadding: EdgeInsetsDirectional.fromSTEB(
                                                                              0.0,
                                                                              0.0,
                                                                              0.0,
                                                                              0.0),
                                                                          color:
                                                                              FlutterFlowTheme.of(context).primary,
                                                                          textStyle: FlutterFlowTheme.of(context)
                                                                              .titleSmall
                                                                              .override(
                                                                                fontFamily: 'Readex Pro',
                                                                                color: Colors.white,
                                                                              ),
                                                                          elevation:
                                                                              3.0,
                                                                          borderSide:
                                                                              BorderSide(
                                                                            color:
                                                                                Colors.transparent,
                                                                            width:
                                                                                1.0,
                                                                          ),
                                                                          borderRadius:
                                                                              BorderRadius.circular(8.0),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    if (_model
                                                                            .addclassheader ??
                                                                        true)
                                                                      Align(
                                                                        alignment: AlignmentDirectional(
                                                                            1.0,
                                                                            -1.0),
                                                                        child:
                                                                            Padding(
                                                                          padding: EdgeInsetsDirectional.fromSTEB(
                                                                              0.0,
                                                                              5.0,
                                                                              0.0,
                                                                              0.0),
                                                                          child:
                                                                              FutureBuilder<ApiCallResponse>(
                                                                            future:
                                                                                GetClassbyEqartCall.call(
                                                                              urlendpoint: FFAppState().urlendpoint,
                                                                              eqart: _model.objectValue,
                                                                            ),
                                                                            builder:
                                                                                (context, snapshot) {
                                                                              // Customize what your widget looks like when it's loading.
                                                                              if (!snapshot.hasData) {
                                                                                return Center(
                                                                                  child: SizedBox(
                                                                                    width: 50.0,
                                                                                    height: 50.0,
                                                                                    child: SpinKitSquareCircle(
                                                                                      color: FlutterFlowTheme.of(context).primary,
                                                                                      size: 50.0,
                                                                                    ),
                                                                                  ),
                                                                                );
                                                                              }
                                                                              final classheaderGetClassbyEqartResponse = snapshot.data!;
                                                                              return FlutterFlowDropDown<String>(
                                                                                controller: _model.classheaderValueController ??= FormFieldController<String>(null),
                                                                                options: (GetClassbyEqartCall.classcode(
                                                                                  classheaderGetClassbyEqartResponse.jsonBody,
                                                                                ) as List)
                                                                                    .map<String>((s) => s.toString())
                                                                                    .toList()!,
                                                                                onChanged: (val) async {
                                                                                  setState(() => _model.classheaderValue = val);
                                                                                  _model.apiResult28e = await GetClassInfoCall.call(
                                                                                    classinfo: _model.classheaderValue,
                                                                                    urlendpoint: FFAppState().urlendpoint,
                                                                                  );
                                                                                  if ((_model.apiResult28e?.succeeded ?? true)) {
                                                                                    setState(() {
                                                                                      _model.classinfo = getJsonField(
                                                                                        (_model.apiResult28e?.jsonBody ?? ''),
                                                                                        r'''$[:]''',
                                                                                      )!
                                                                                          .toList()
                                                                                          .cast<dynamic>();
                                                                                    });
                                                                                  }

                                                                                  setState(() {});
                                                                                },
                                                                                width: 300.0,
                                                                                height: 50.0,
                                                                                searchHintTextStyle: FlutterFlowTheme.of(context).labelMedium.override(
                                                                                      fontFamily: 'Readex Pro',
                                                                                      color: FlutterFlowTheme.of(context).tertiary,
                                                                                    ),
                                                                                textStyle: FlutterFlowTheme.of(context).bodyMedium,
                                                                                hintText: 'Click to select 1 class header only..',
                                                                                searchHintText: 'Search for an item...',
                                                                                icon: Icon(
                                                                                  Icons.keyboard_arrow_down_rounded,
                                                                                  color: FlutterFlowTheme.of(context).secondaryText,
                                                                                  size: 24.0,
                                                                                ),
                                                                                elevation: 2.0,
                                                                                borderColor: Color(0x9CABABAB),
                                                                                borderWidth: 2.0,
                                                                                borderRadius: 8.0,
                                                                                margin: EdgeInsetsDirectional.fromSTEB(16.0, 4.0, 16.0, 4.0),
                                                                                hidesUnderline: true,
                                                                                isSearchable: true,
                                                                              );
                                                                            },
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    Container(
                                                                      decoration:
                                                                          BoxDecoration(),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Builder(
                                                              builder:
                                                                  (context) {
                                                                final classinf =
                                                                    _model
                                                                        .classinfo
                                                                        .toList();
                                                                if (classinf
                                                                    .isEmpty) {
                                                                  return Image
                                                                      .asset(
                                                                    'assets/images/unnamed.png',
                                                                  );
                                                                }
                                                                return SingleChildScrollView(
                                                                  primary:
                                                                      false,
                                                                  child: Column(
                                                                    mainAxisSize:
                                                                        MainAxisSize
                                                                            .min,
                                                                    children: List.generate(
                                                                        classinf
                                                                            .length,
                                                                        (classinfIndex) {
                                                                      final classinfItem =
                                                                          classinf[
                                                                              classinfIndex];
                                                                      return Container(
                                                                        decoration:
                                                                            BoxDecoration(),
                                                                        child:
                                                                            Row(
                                                                          mainAxisSize:
                                                                              MainAxisSize.max,
                                                                          children: [
                                                                            Container(
                                                                              width: MediaQuery.sizeOf(context).width * 0.4,
                                                                              decoration: BoxDecoration(),
                                                                              child: Column(
                                                                                mainAxisSize: MainAxisSize.max,
                                                                                children: [
                                                                                  Padding(
                                                                                    padding: EdgeInsetsDirectional.fromSTEB(10.0, 10.0, 10.0, 10.0),
                                                                                    child: AutoSizeText(
                                                                                      getJsonField(
                                                                                        classinfItem,
                                                                                        r'''$.CharName''',
                                                                                      ).toString(),
                                                                                      textAlign: TextAlign.center,
                                                                                      style: FlutterFlowTheme.of(context).bodyMedium.override(
                                                                                            fontFamily: 'Readex Pro',
                                                                                            fontSize: 14.0,
                                                                                          ),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                            Container(
                                                                              width: MediaQuery.sizeOf(context).width * 0.4,
                                                                              decoration: BoxDecoration(),
                                                                              child: Align(
                                                                                alignment: AlignmentDirectional(1.0, -1.0),
                                                                                child: wrapWithModel(
                                                                                  model: _model.textfieldModels.getModel(
                                                                                    classinfItem.toString(),
                                                                                    classinfIndex,
                                                                                  ),
                                                                                  updateCallback: () => setState(() {}),
                                                                                  updateOnChange: true,
                                                                                  child: TextfieldWidget(
                                                                                    key: Key(
                                                                                      'Keyubw_${classinfItem.toString()}',
                                                                                    ),
                                                                                    charname: getJsonField(
                                                                                      classinfItem,
                                                                                      r'''$.CharName''',
                                                                                    ).toString(),
                                                                                    classID: getJsonField(
                                                                                      classinfItem,
                                                                                      r'''$.ClassID''',
                                                                                    ).toString(),
                                                                                    charID: getJsonField(
                                                                                      classinfItem,
                                                                                      r'''$.CharID''',
                                                                                    ).toString(),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      );
                                                                    }),
                                                                  ),
                                                                );
                                                              },
                                                            ),
                                                          ],
                                                        ),
                                                        Column(
                                                          mainAxisSize:
                                                              MainAxisSize.max,
                                                          children: [
                                                            Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .max,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Align(
                                                                  alignment:
                                                                      AlignmentDirectional(
                                                                          0.0,
                                                                          -1.0),
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsetsDirectional
                                                                        .fromSTEB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0),
                                                                    child:
                                                                        FFButtonWidget(
                                                                      onPressed:
                                                                          () async {
                                                                        final selectedMedia =
                                                                            await selectMedia(
                                                                          multiImage:
                                                                              false,
                                                                        );
                                                                        if (selectedMedia !=
                                                                                null &&
                                                                            selectedMedia.every((m) =>
                                                                                validateFileFormat(m.storagePath, context))) {
                                                                          setState(() =>
                                                                              _model.isDataUploading1 = true);
                                                                          var selectedUploadedFiles =
                                                                              <FFUploadedFile>[];

                                                                          try {
                                                                            selectedUploadedFiles = selectedMedia
                                                                                .map((m) => FFUploadedFile(
                                                                                      name: m.storagePath.split('/').last,
                                                                                      bytes: m.bytes,
                                                                                      height: m.dimensions?.height,
                                                                                      width: m.dimensions?.width,
                                                                                      blurHash: m.blurHash,
                                                                                    ))
                                                                                .toList();
                                                                          } finally {
                                                                            _model.isDataUploading1 =
                                                                                false;
                                                                          }
                                                                          if (selectedUploadedFiles.length ==
                                                                              selectedMedia.length) {
                                                                            setState(() {
                                                                              _model.uploadedLocalFile1 = selectedUploadedFiles.first;
                                                                            });
                                                                          } else {
                                                                            setState(() {});
                                                                            return;
                                                                          }
                                                                        }

                                                                        setState(
                                                                            () {
                                                                          _model
                                                                              .addToUploadedfile(_model.uploadedLocalFile1);
                                                                        });
                                                                      },
                                                                      text:
                                                                          'Camera',
                                                                      options:
                                                                          FFButtonOptions(
                                                                        height:
                                                                            40.0,
                                                                        padding: EdgeInsetsDirectional.fromSTEB(
                                                                            24.0,
                                                                            0.0,
                                                                            24.0,
                                                                            0.0),
                                                                        iconPadding: EdgeInsetsDirectional.fromSTEB(
                                                                            0.0,
                                                                            0.0,
                                                                            0.0,
                                                                            0.0),
                                                                        color: FlutterFlowTheme.of(context)
                                                                            .primary,
                                                                        textStyle: FlutterFlowTheme.of(context)
                                                                            .titleSmall
                                                                            .override(
                                                                              fontFamily: 'Readex Pro',
                                                                              color: Colors.white,
                                                                            ),
                                                                        elevation:
                                                                            3.0,
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              Colors.transparent,
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8.0),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Align(
                                                                  alignment:
                                                                      AlignmentDirectional(
                                                                          0.0,
                                                                          -1.0),
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsetsDirectional
                                                                        .fromSTEB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0),
                                                                    child:
                                                                        FFButtonWidget(
                                                                      onPressed:
                                                                          () async {
                                                                        final selectedMedia =
                                                                            await selectMedia(
                                                                          mediaSource:
                                                                              MediaSource.photoGallery,
                                                                          multiImage:
                                                                              false,
                                                                        );
                                                                        if (selectedMedia !=
                                                                                null &&
                                                                            selectedMedia.every((m) =>
                                                                                validateFileFormat(m.storagePath, context))) {
                                                                          setState(() =>
                                                                              _model.isDataUploading2 = true);
                                                                          var selectedUploadedFiles =
                                                                              <FFUploadedFile>[];

                                                                          try {
                                                                            selectedUploadedFiles = selectedMedia
                                                                                .map((m) => FFUploadedFile(
                                                                                      name: m.storagePath.split('/').last,
                                                                                      bytes: m.bytes,
                                                                                      height: m.dimensions?.height,
                                                                                      width: m.dimensions?.width,
                                                                                      blurHash: m.blurHash,
                                                                                    ))
                                                                                .toList();
                                                                          } finally {
                                                                            _model.isDataUploading2 =
                                                                                false;
                                                                          }
                                                                          if (selectedUploadedFiles.length ==
                                                                              selectedMedia.length) {
                                                                            setState(() {
                                                                              _model.uploadedLocalFile2 = selectedUploadedFiles.first;
                                                                            });
                                                                          } else {
                                                                            setState(() {});
                                                                            return;
                                                                          }
                                                                        }

                                                                        setState(
                                                                            () {
                                                                          _model
                                                                              .addToUploadedfile(_model.uploadedLocalFile2);
                                                                        });
                                                                      },
                                                                      text:
                                                                          'Gallery',
                                                                      options:
                                                                          FFButtonOptions(
                                                                        height:
                                                                            40.0,
                                                                        padding: EdgeInsetsDirectional.fromSTEB(
                                                                            24.0,
                                                                            0.0,
                                                                            24.0,
                                                                            0.0),
                                                                        iconPadding: EdgeInsetsDirectional.fromSTEB(
                                                                            0.0,
                                                                            0.0,
                                                                            0.0,
                                                                            0.0),
                                                                        color: FlutterFlowTheme.of(context)
                                                                            .primary,
                                                                        textStyle: FlutterFlowTheme.of(context)
                                                                            .titleSmall
                                                                            .override(
                                                                              fontFamily: 'Readex Pro',
                                                                              color: Colors.white,
                                                                            ),
                                                                        elevation:
                                                                            3.0,
                                                                        borderSide:
                                                                            BorderSide(
                                                                          color:
                                                                              Colors.transparent,
                                                                          width:
                                                                              1.0,
                                                                        ),
                                                                        borderRadius:
                                                                            BorderRadius.circular(8.0),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            Expanded(
                                                              child: Builder(
                                                                builder:
                                                                    (context) {
                                                                  final photolocal = _model
                                                                      .uploadedfile
                                                                      .toList()
                                                                      .take(9)
                                                                      .toList();
                                                                  if (photolocal
                                                                      .isEmpty) {
                                                                    return Image
                                                                        .asset(
                                                                      'assets/images/n9qxe_R.jpg',
                                                                    );
                                                                  }
                                                                  return GridView
                                                                      .builder(
                                                                    padding:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    gridDelegate:
                                                                        SliverGridDelegateWithFixedCrossAxisCount(
                                                                      crossAxisCount:
                                                                          3,
                                                                      crossAxisSpacing:
                                                                          10.0,
                                                                      mainAxisSpacing:
                                                                          10.0,
                                                                      childAspectRatio:
                                                                          1.0,
                                                                    ),
                                                                    shrinkWrap:
                                                                        true,
                                                                    scrollDirection:
                                                                        Axis.vertical,
                                                                    itemCount:
                                                                        photolocal
                                                                            .length,
                                                                    itemBuilder:
                                                                        (context,
                                                                            photolocalIndex) {
                                                                      final photolocalItem =
                                                                          photolocal[
                                                                              photolocalIndex];
                                                                      return Container(
                                                                        width: MediaQuery.sizeOf(context).width *
                                                                            1.0,
                                                                        height: MediaQuery.sizeOf(context).height *
                                                                            1.0,
                                                                        child: custom_widgets
                                                                            .Imagebyte(
                                                                          width:
                                                                              MediaQuery.sizeOf(context).width * 1.0,
                                                                          height:
                                                                              MediaQuery.sizeOf(context).height * 1.0,
                                                                          image:
                                                                              photolocalItem,
                                                                        ),
                                                                      );
                                                                    },
                                                                  );
                                                                },
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
