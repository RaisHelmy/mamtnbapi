import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'lat_lng.dart';
import 'place.dart';
import 'uploaded_file.dart';

String? dateformat(String? originalDate) {
  if (originalDate != null) {
    DateTime dateTime = DateTime.parse(originalDate);
    String formattedDate = DateFormat('M/d/yyyy h:mm:ss a').format(dateTime);
    return formattedDate;
  }
}

bool? isitsamelist(
  String? stringA,
  List<String>? stringB,
) {
  if (stringA != null && stringB != null) {
    final trimmedA = stringA.trim();
    for (String item in stringB) {
      final trimmedItem = item.trim();
      if (trimmedA == trimmedItem) {
        return true;
      }
    }
    return false;
  }
  return null;
}

List<dynamic>? distinctlist(dynamic test) {
  List<String> inputList = List<String>.from(test);
  List<String> distinctList = inputList.toSet().toList();
  return distinctList;
}

String? colortohex(String colorName) {
  String hexColor = '';
  switch (colorName.toLowerCase()) {
    case 'red':
      hexColor = '#FF0000';
      break;
    case 'green':
      hexColor = '#00FF00';
      break;
    case 'blue':
      hexColor = '#0000FF';
      break;
    case 'yellow':
      hexColor = '#FFFF00';
      break;
    case 'orange':
      hexColor = '#FFA500';
      break;
    case 'purple':
      hexColor = '#800080';
      break;
    case 'pink':
      hexColor = '#FFC0CB';
      break;
    case 'brown':
      hexColor = '#A52A2A';
      break;
    case 'black':
      hexColor = '#000000';
      break;
    case 'white':
      hexColor = '#FFFFFF';
      break;
    default:
      print('Color not found!');
  }
  return hexColor;
}

dynamic tokentojson(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final username = payloadData['sub'].toString().replaceAll('\\\\', '\\');
    final expiration = payloadData['exp'] as int?;

    if (username != null && expiration != null) {
      final expirationDate =
          DateTime.fromMillisecondsSinceEpoch(expiration * 1000);
      final formattedExpiration =
          DateFormat.yMMMMd().add_jms().format(expirationDate);
      final timeAgo = timeago.format(expirationDate);

      final result = [
        {
          'username': username,
          'expiration': formattedExpiration,
        }
      ];
      final jsonString = json.encode(result);
      return jsonString;
    }
  } catch (e) {
    print('Error parsing JWT token: $e');
  }
  return null;
}

String? uriencode(String input) {
  try {
    final encodedInput = Uri.encodeComponent(input);
    return encodedInput;
  } catch (e) {
    print('Error encoding input: $e');
  }
  return null;
}

String? tokentousername(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final username = payloadData['sub'].toString().replaceAll('\\\\', '\\');
    return username;
  } catch (e) {
    print('Error parsing JWT token: $e');
  }

  return null;
}

DateTime? tokentoexpieration(String token) {
  try {
    final parts = token.split('.');
    final payload = parts[1];

    final normalizedPayload = base64Url.normalize(payload);
    final decodedPayload = utf8.decode(base64Url.decode(normalizedPayload));

    final payloadData = json.decode(decodedPayload);

    final expiration = payloadData['exp'] as int?;

    if (expiration != null) {
      final expirationDate =
          DateTime.fromMillisecondsSinceEpoch(expiration * 1000);

      return expirationDate;
    }
  } catch (e) {
    print('Error parsing JWT token: $e');
  }
  return null;
}

String doubletosingleslash(String doubleslash) {
  return doubleslash.replaceAll('\\\\', '\\');
}

List<String>? combinelist(
  List<String>? code,
  List<String>? desc,
  String? separator,
) {
  List<String>? output = [];

  if (code == null || desc == null || separator == null) {
    return null;
  }

  int minLength = math.min(code.length, desc.length);

  for (int i = 0; i < minLength; i++) {
    output.add("${code[i]} $separator ${desc[i]}");
  }

  return output;
}

List<dynamic> generatemonth() {
  List<Map<String, String>> monthList = [];

  for (int month = 12; month >= 1; month--) {
    DateTime date = DateTime(2023, month);
    String monthValue = DateFormat('MM').format(date);
    String monthName = DateFormat('MMMM').format(date);

    monthList.add({'value': monthValue, 'name': monthName});
  }

  return monthList;
}

String encodeparameter2(
  String uid,
  String? fl,
  String? fldesc,
  String? eqdesc,
  String? manufacturer,
  String? modelno,
  String? serialno,
  String? objcode,
  String? objdesc,
  String? startupdate,
  String? constructyear,
  String? constructmonth,
  String? countrycode,
  String? countryname,
  String? businessarea,
  String? assetno,
  String? assetsubno,
  String? costcenter,
  String? plannergroupcode,
  String? plannergroupdesc,
  String? mainworkcentercode,
  String? mainworkcenterdesc,
  String? eqno,
  List<String> classheadercode,
  List<String> charcodes,
  List<String> charvalues,
  List<String>? classheadercode2,
  List<String>? charcodes2,
  List<String>? charvalues2,
) {
  // Function to remove spaces from each element in the classheadercode list
  List<String> removeSpacesFromClassheadercode(List<String>? inputList) {
    return inputList?.map((item) => item.replaceAll(' ', '')).toList() ?? [];
  }

  // Create new lists to hold the combined values
  final combinedClassheadercode =
      removeSpacesFromClassheadercode(classheadercode);
  final combinedCharcodes = charcodes;
  final combinedCharvalues = charvalues;

  // Handle classheadercode2, charcodes2, and charvalues2 when they are non-null and non-empty
  if (classheadercode2 != null && classheadercode2.isNotEmpty) {
    combinedClassheadercode
        .addAll(removeSpacesFromClassheadercode(classheadercode2));
  }
  if (charcodes2 != null && charcodes2.isNotEmpty) {
    combinedCharcodes.addAll(charcodes2);
  }
  if (charvalues2 != null && charvalues2.isNotEmpty) {
    combinedCharvalues.addAll(charvalues2);
  }

  final Map<String, dynamic> parameters = {
    'uid': uid,
    'fl': fl,
    'fldesc': fldesc,
    'eqdesc': eqdesc,
    'manufacturer': manufacturer,
    'modelno': modelno,
    'serialno': serialno,
    'objcode': objcode,
    'objdesc': objdesc,
    'startupdate': startupdate,
    'constructyear': constructyear,
    'constructmonth': constructmonth,
    'countrycode': countrycode,
    'countryname': countryname,
    'businessarea': businessarea,
    'assetno': assetno,
    'assetsubno': assetsubno,
    'costcenter': costcenter,
    'plannergroupcode': plannergroupcode,
    'plannergroupdesc': plannergroupdesc,
    'mainworkcentercode': mainworkcentercode,
    'mainworkcenterdesc': mainworkcenterdesc,
    'eqno': eqno,
    'classheadercodes': combinedClassheadercode,
    'charcodes': combinedCharcodes,
    'charvalues': combinedCharvalues,
  };
  final encodedParameters = Uri(queryParameters: parameters).query;
  return encodedParameters;
}

List<String> generateday(
  String year,
  String month,
) {
  int selectedYear = int.parse(year);
  int selectedMonth = int.parse(month);

  DateTime startDate = DateTime(selectedYear, selectedMonth, 1);
  DateTime endDate = DateTime(selectedYear, selectedMonth + 1, 0);

  List<String> dayList = [];

  for (int day = 1; day <= endDate.day; day++) {
    DateTime date = DateTime(selectedYear, selectedMonth, day);
    String formattedDay = DateFormat('dd').format(date);

    dayList.add(formattedDay);
  }

  return dayList;
}

List<String> generatefromyear(int fromYear) {
  DateTime now = DateTime.now();
  int currentYear = now.year;
  List<String> yearList = [];

  for (int year = currentYear; year >= fromYear; year--) {
    yearList.add(year.toString());
  }

  return yearList;
}

String inbdttoday(String inbdt) {
  String inbdtString = inbdt.toString();
  String dayString = inbdtString.substring(6, 8);
  String formattedDay = dayString.padLeft(2, '0');
  return formattedDay;
}

String inbdttomonth(String inbdt) {
  String inbdtString = inbdt.toString();
  String monthString = inbdtString.substring(4, 6);
  String formattedMonth = monthString.padLeft(2, '0');
  return formattedMonth;
}

List<String>? prefixlist(List<dynamic>? objecttype) {
  return objecttype
      ?.map((obj) =>
          obj != null ? (obj["ObjectTypePrefix"]?.toString() ?? '') : '')
      .toList();
}

List<bool>? checklistsamestring(
  List<String>? list,
  String? string,
) {
  List<bool>? output = [];

  if (list == null || string == null) {
    return null;
  }

  for (int i = 0; i < list.length; i++) {
    output.add(list[i] == string);
  }

  return output;
}

int? findindex(
  List<String>? listitem,
  String? value,
) {
  return null;
}

bool checkjsoncontainword(
  dynamic jsondata,
  String? value,
) {
  String jsonDataString = json
      .encode(jsondata)
      .toLowerCase(); // Convert the jsondata to lowercase String
  value = value?.toLowerCase(); // Convert the value to lowercase
  return jsonDataString.contains(value ??
      ''); // Check if the lowercase String contains the lowercase value
}

String? prefix(dynamic objecttype) {
  return objecttype["ObjectTypePrefix"] ?? '';
}

int inbdttoyear(String inbdt) {
  String inbdtString = inbdt.toString();
  String yearString = inbdtString.substring(0, 4);
  int year = int.parse(yearString);
  return year;
}

List<dynamic> jsonfromcharid(
  List<dynamic> listA,
  String listB,
) {
  List<dynamic> filteredList =
      listA.where((item) => item['CharID'].trim() == listB.trim()).toList();
  return filteredList;
}

List<int>? generateyear(
  int fromYear,
  int toYear,
) {
  List<int> yearList = [];
  for (int year = fromYear; year <= toYear; year++) {
    yearList.add(year);
  }

  return yearList;
}

List<dynamic> filterlistobjecttypebylisteqart(
  List<dynamic> listA,
  List<String> listB,
) {
  List<String> trimmedListB = listB.map((item) => item.trim()).toList();
  List<dynamic> filteredList = listA.where((item) {
    return trimmedListB.contains(item['CLASS'].trim());
  }).toList();
  return filteredList;
}

List<dynamic> jsonfromcode(
  List<dynamic> listA,
  String listB,
) {
  List<dynamic> filteredList =
      listA.where((item) => item['Code'].trim() == listB.trim()).toList();
  return filteredList;
}

String guid() {
  final random = math.Random();
  final charSet =
      '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  final segments = [8, 4, 4, 4, 12];
  final buffer = StringBuffer();

  for (final segmentLength in segments) {
    for (var i = 0; i < segmentLength; i++) {
      final randomIndex = random.nextInt(charSet.length);
      buffer.write(charSet[randomIndex]);
    }
    buffer.write('-');
  }

  final guid = buffer.toString().substring(0, buffer.length - 1);
  return guid;
}

String addprefixtnb(
  String userid,
  String prefix,
) {
  if (userid != null && !userid.startsWith(prefix ?? '')) {
    return '$prefix$userid';
  }
  return userid;
}

String removespace(String somevalue) {
  String withoutSpaces = somevalue.replaceAll(' ', '');
  return withoutSpaces;
}

String? headercombinefromlist(
  List<String>? charcodes,
  List<String>? charvalues,
) {
  if (charcodes == null || charvalues == null) {
    return null;
  }
  if (charcodes.length != charvalues.length) {
    throw Exception('charcodes and charvalues must have the same length');
  }

  final items = <String>[];
  for (var i = 0; i < charcodes.length; i++) {
    final charcode = charcodes[i];
    final charvalue = charvalues[i];

    if (charcode.isNotEmpty && charvalue.isNotEmpty) {
      items.add('charcodes=${Uri.encodeQueryComponent(charcode)}');
      items.add('charvalues=${Uri.encodeQueryComponent(charvalue)}');
    }
  }

  if (items.isEmpty) {
    return null;
  }

  return '&${items.join('&')}';
}

String encodeparameter(
  String uid,
  String? fl,
  String? fldesc,
  String? eqdesc,
  String? manufacturer,
  String? modelno,
  String? serialno,
  String? objcode,
  String? objdesc,
  String? startupdate,
  String? constructyear,
  String? constructmonth,
  String? countrycode,
  String? countryname,
  String? businessarea,
  String? assetno,
  String? assetsubno,
  String? costcenter,
  String? plannergroupcode,
  String? plannergroupdesc,
  String? mainworkcentercode,
  String? mainworkcenterdesc,
  String? eqno,
  String? classheadercode,
  List<String>? charcodes,
  List<String>? charvalues,
) {
  final Map<String, dynamic> parameters = {
    'uid': uid,
    'fl': fl,
    'fldesc': fldesc,
    'eqdesc': eqdesc,
    'manufacturer': manufacturer,
    'modelno': modelno,
    'serialno': serialno,
    'objcode': objcode,
    'objdesc': objdesc,
    'startupdate': startupdate,
    'constructyear': constructyear,
    'constructmonth': constructmonth,
    'countrycode': countrycode,
    'countryname': countryname,
    'businessarea': businessarea,
    'assetno': assetno,
    'assetsubno': assetsubno,
    'costcenter': costcenter,
    'plannergroupcode': plannergroupcode,
    'plannergroupdesc': plannergroupdesc,
    'mainworkcentercode': mainworkcentercode,
    'mainworkcenterdesc': mainworkcenterdesc,
    'eqno': eqno,
    'classheadercode': classheadercode,
    'charcodes': charcodes,
    'charvalues': charvalues,
  };
  final encodedParameters = Uri(queryParameters: parameters).query;
  return encodedParameters;
}

List<String>? optionvalue(List<String>? optioninput) {
  List<String>? outputList = [];

  if (optioninput != null) {
    for (String? input in optioninput) {
      if (input?.trim().toLowerCase().contains('e') == true) {
        double? value = double.tryParse(input!);
        if (value != null) {
          outputList.add(value.toInt().toString());
        } else {
          outputList.add(input!);
        }
      } else {
        outputList.add(input!);
      }
    }
  }

  return outputList;
}

List<String>? fromlisttolist(
  List<String>? listA,
  List<String>? listB,
) {
  List<String>? outputList = [];

  if (listA != null && listB != null) {
    for (int i = 0; i < listA.length; i++) {
      String? elementA = listA[i];
      String? elementB = listB[i];

      if (elementA != null && elementA.isNotEmpty) {
        outputList.add(elementA);
      } else if (elementB != null && elementB.isNotEmpty) {
        outputList.add(elementB);
      } else {
        outputList.add('');
      }
    }
  }

  return outputList;
}

List<String>? distinct(List<String>? list) {
  if (list == null) {
    return null;
  }

  Set<String> distinctSet = Set<String>.from(list);
  List<String> distinctList = distinctSet.toList();

  return distinctList;
}

bool? isitsame(
  String? stringA,
  String? stringB,
) {
  if (stringA != null && stringB != null) {
    final trimmedA = stringA.trim();
    final trimmedB = stringB.trim();
    return trimmedA == trimmedB;
  }
  return null;
}

String? flfromdropdown(
  String? substation,
  String? bay,
  String? primarysecondary,
  String? object,
) {
  List<String?> parts = [];

  if (bay != null && bay.trim().isNotEmpty) {
    parts.add(bay);
    if (primarysecondary != null && primarysecondary.trim().isNotEmpty) {
      parts.add(primarysecondary);
      if (object != null && object.trim().isNotEmpty) {
        parts.add(object);
      }
    }
  } else if (substation != null && substation.trim().isNotEmpty) {
    parts.add(substation);
    if (bay != null && bay.trim().isNotEmpty) {
      parts.add(bay);
      if (primarysecondary != null && primarysecondary.trim().isNotEmpty) {
        parts.add(primarysecondary);
        if (object != null && object.trim().isNotEmpty) {
          parts.add(object);
        }
      }
    }
  }

  return parts.join('/');
}

List<dynamic>? classinfofromequipmentandclassresponse(
  List<dynamic> equipmentresponse,
  String classheadercode,
  List<dynamic> characteristicinfo,
) {
  // Step 1: Filter input A based on input B
  List<dynamic> filteredEquipment = equipmentresponse
      .where((equipment) =>
          equipment["ClassHeaderCode"].trim() == classheadercode.trim())
      .toList();

  // Step 2: Add info from input C to the filtered equipment
  for (var equipment in filteredEquipment) {
    var characteristicCode = equipment["CharacteristicCode"];
    var classHeaderCode = equipment["ClassHeaderCode"].trim();

    var matchingCharacteristics = characteristicinfo
        ?.where((characteristic) =>
            characteristic["CLASS"].trim() == classHeaderCode &&
            characteristic["CharID"] == characteristicCode)
        .toList();

    if (matchingCharacteristics != null && matchingCharacteristics.isNotEmpty) {
      // Assuming there's only one matching characteristic in input C
      equipment.addAll(matchingCharacteristics[0]);
    }
  }

  // Step 3: Return the final list
  return filteredEquipment;
}

List<String>? emptylist() {
  return null;
}
