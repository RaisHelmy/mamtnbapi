import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class GridviewuploadedimageModel extends FlutterFlowModel {
  ///  Local state fields for this component.

  List<FFUploadedFile> listofphoto = [];
  void addToListofphoto(FFUploadedFile item) => listofphoto.add(item);
  void removeFromListofphoto(FFUploadedFile item) => listofphoto.remove(item);
  void removeAtIndexFromListofphoto(int index) => listofphoto.removeAt(index);
  void updateListofphotoAtIndex(int index, Function(FFUploadedFile) updateFn) =>
      listofphoto[index] = updateFn(listofphoto[index]);

  /// Initialization and disposal methods.

  void initState(BuildContext context) {}

  void dispose() {}

  /// Action blocks are added here.

  /// Additional helper methods are added here.
}
